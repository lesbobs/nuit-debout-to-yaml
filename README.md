# nuit-debout-to-yaml

Convert Nuit Debout catalog of services to a Git repository of YAML files.
Link to catalog : https://wiki.nuitdebout.fr/wiki/Ressources/Liste_d%27outils_num%C3%A9riques


## Usage

```bash
./nuit_debout_to_yaml.py ../nuit_debout_yaml/
```

## Installing packages for Ubuntu 16.04
In bash
sudo app-get install python3-pip
sudo app-get install python3-yaml
sudo app-get install python3-lxml

If you use python, replace all python 3 by python

## Installing dependencies

```bash
virtualenv --python=python3 v
source v/bin/activate
pip install -r requirements.txt
```



